# 实验4：PL/SQL语言打印杨辉三角
- 学号：202010414323 姓名：杨杰 班级：软件工程3班
## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriangle，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriangle的SQL语句。

## 杨辉三角源代码

```sql
set serveroutput on;
declare
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
N integer := 9; -- 一共打印9行数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;
/
```
![](./1.png)
## 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriangle，存储起来。存储过程接受行数N作为参数。
```sql
CREATE OR REPLACE PROCEDURE YHTriangle(N IN INTEGER) AS
    type t_number is varray (100) of integer not null; --数组
    i integer;
    j integer;
    spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
    rowArray t_number := t_number();
BEGIN
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1, 9, ' '));--先打印第2行
    dbms_output.put(rpad(1, 9, ' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j), 9, ' '));--打印数字并在后面补空格
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;

/
```
![](./2.png)
## 创建YHTriangle的存储过程
![](./4.png)
## 运行这个存储过程即可以打印出N行杨辉三角。
```sql
SET SERVEROUTPUT ON;
BEGIN
YHTriangle(5);
END;
```
调用`YHTriangle`的存储过程，并传入参数`N=5`，该过程的作用是打印出一个杨辉三角（YHTriangle）的前5行。![](./3.png)


## 实验总结
通过本次实验，加深了对于PL/SQL语言的了解
PL/SQL编程包括变量声明、控制结构、循环结构和函数和过程等。PL/SQL程序可以使用Oracle SQL语言来访问和操作数据库对象，例如表、视图、触发器等。
