# 实验5：包，过程，函数的用法

- 学号：202010414323   姓名：杨杰   班级：软件工程三班

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```

![](./1.png)

## 脚本代码

```sql
create or replace PACKAGE MyPack IS
/*
本实验以实验4为基础。
包MyPack中有：
一个函数:Get_SalaryAmount(V_DEPARTMENT_ID NUMBER)，
一个过程:Get_Employees(V_EMPLOYEE_ID NUMBER)
*/
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/
```

![](./2.png)

![](./3.png)

## 测试

函数Get_SalaryAmount()测试方法：

```sql

select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;
```

![](./4.png)

过程Get_Employees()测试代码：

```sql
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
/
```

![](./5.png)

## 实验总结

在这次的实验中，我深入了解了PL/SQL变量和常量的声明和使用方法，以及包、过程、函数的用法。通过实践，我对PL/SQL编程有了更深入的了解，对于维护Oracle数据库系统和开发面向数据库的应用程序有更多的自信。

通过定义变量和常量，我能够使用它们进行各种运算或处理，例如赋值、比较、操作等。这样可以更灵活地进行编程，实现更多的功能和自定义维护。

在SQL编程中，包是非常有用的，将相关的变量、常量、类型定义、过程、函数封装在一起，使其可以在整个数据库上下文中被共享和重用。定义规范和主体有助于团队合作，提高了代码的可读性和可维护性。而过程和函数则可以完成更为复杂的任务，提高了应用程序和数据库之间交互的效率和准确性。

在实践中，我遇到了一些问题，例如变量和常量的作用域需要特别注意，包的定义需要严格按照规定构建，过程和函数需要考虑输入输出变量等技术难点。学习PL/SQL编程需要更为专业的知识和技能，需要不断地实践和学习。

总的来说，PL/SQL编程是数据库系统应用开发非常重要的一环，学习其中的技术难点和实践方法有助于提高数据库系统的管理能力和应用程序的开发能力，具有非常广泛的应用前景和价值。