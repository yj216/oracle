CREATE TABLESPACE data_tbs
DATAFILE 'data01.dbf'
SIZE 100M AUTOEXTEND ON NEXT 100M
MAXSIZE UNLIMITED;

CREATE TABLESPACE index_tbs
DATAFILE 'index01.dbf'
SIZE 50M AUTOEXTEND ON NEXT 50M
MAXSIZE UNLIMITED;


CREATE TABLE products (
  product_id   NUMBER(10) PRIMARY KEY,
  product_name VARCHAR2(100) NOT NULL,
  price        NUMBER(10, 2) NOT NULL,
  description  CLOB,
  image_url    VARCHAR2(255),
  date_added   DATE NOT NULL
) TABLESPACE data_tbs;

CREATE TABLE orders (
  order_id    NUMBER(10) PRIMARY KEY,
  customer_id NUMBER(10) REFERENCES customers(customer_id),
  order_date  DATE DEFAULT SYSDATE NOT NULL
) TABLESPACE data_tbs;

CREATE TABLE customers (
  customer_id NUMBER(10) PRIMARY KEY,
  first_name  VARCHAR2(50) NOT NULL,
  last_name   VARCHAR2(50) NOT NULL,
  email       VARCHAR2(255) UNIQUE NOT NULL,
  phone       VARCHAR2(20)
) TABLESPACE data_tbs;

CREATE TABLE order_items (
  order_id    NUMBER(10) REFERENCES orders(order_id),
  product_id  NUMBER(10) REFERENCES products(product_id),
  quantity    NUMBER(10) NOT NULL,
  price       NUMBER(10, 2) NOT NULL,
  CONSTRAINT order_items_pk PRIMARY KEY (order_id, product_id)
) TABLESPACE data_tbs;


BEGIN
FOR i IN 1..100000 LOOP
INSERT INTO products (product_id, product_name, price, quantity)
VALUES (i, 'Product ' || i, ROUND(DBMS_RANDOM.VALUE(10, 1000), 2), ROUND(DBMS_RANDOM.VALUE(1, 100), 0));
END LOOP;
COMMIT;
END;
/
BEGIN
FOR i IN 1..100000 LOOP
INSERT INTO customers (customer_id, customer_name, contact, address)
VALUES (i, 'Customer ' || i, 'Contact ' || i, 'Address ' || i);
END LOOP;
COMMIT;
END;
/

BEGIN
FOR i IN 1..100000 LOOP
INSERT INTO orders (order_id, customer_id, order_date, total_amount)
VALUES (i, ROUND(DBMS_RANDOM.VALUE(1, 100000), 0), SYSDATE - i, ROUND(DBMS_RANDOM.VALUE(10, 1000), 2));
END LOOP;
COMMIT;
END;
/

BEGIN
FOR i IN 1..100000 LOOP
DECLARE
product_id NUMBER;
BEGIN
SELECT product_id
INTO product_id
FROM products
WHERE product_id = ROUND(DBMS_RANDOM.VALUE(1, 100000), 0);
  INSERT INTO order_details (detail_id, order_id, product_id, quantity, unit_price, total_amount)
  VALUES (i, ROUND(DBMS_RANDOM.VALUE(1, 100000), 0), product_id, ROUND(DBMS_RANDOM.VALUE(1, 10), 0), ROUND(DBMS_RANDOM.VALUE(10, 100), 2), ROUND(DBMS_RANDOM.VALUE(10, 1000), 2));
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    CONTINUE;
END;
END LOOP;
COMMIT;
END;
/


