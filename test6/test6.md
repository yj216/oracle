﻿﻿﻿﻿﻿﻿﻿# 实验6（期末考核） 基于Oracle数据库的商品销售系统

**姓名：杨杰**

**学号：202010414323**

**班级：20软工三班**

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
- 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
- 设计权限及用户分配方案。至少两个用户。
- 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
- 设计一套数据库的备份方案。

## 实验内容

1. 创建用户,授予用户权限
```
GRANT USER yj IDENTIFIED BY 1234;  
GRANT CONNECT,RESOURCE,CREATE SESSION TO yj;
```
![](./1.png)


2. 创建表空间
本文将创建两个表空间：DATA和INDEX，用于存储表数据和索引数据。我们将在DATA表空间中创建PRODUCTS和ORDERS表，在INDEX表空间中创建PRODUCTS_IDX和ORDERS_IDX索引表。

我们使用以下语句创建表空间：
```
CREATE TABLESPACE data_tbs
DATAFILE 'data01.dbf'
SIZE 100M AUTOEXTEND ON NEXT 100M
MAXSIZE UNLIMITED;

CREATE TABLESPACE index_tbs
DATAFILE 'index01.dbf'
SIZE 50M AUTOEXTEND ON NEXT 50M
MAXSIZE UNLIMITED;
```
![](./2.png)


3. 接下来，我们创建四张表：

PRODUCTS - 存储商品信息
ORDERS - 存储订单信息
CUSTOMERS - 存储客户信息
ORDER_ITEMS - 存储订单商品项信息  
```
CREATE TABLE products (
  product_id   NUMBER(10) PRIMARY KEY,
  product_name VARCHAR2(100) NOT NULL,
  price        NUMBER(10, 2) NOT NULL,
  description  CLOB,
  image_url    VARCHAR2(255),
  date_added   DATE NOT NULL
) TABLESPACE data_tbs;

CREATE TABLE orders (
  order_id    NUMBER(10) PRIMARY KEY,
  customer_id NUMBER(10) REFERENCES customers(customer_id),
  order_date  DATE DEFAULT SYSDATE NOT NULL
) TABLESPACE data_tbs;

CREATE TABLE customers (
  customer_id NUMBER(10) PRIMARY KEY,
  first_name  VARCHAR2(50) NOT NULL,
  last_name   VARCHAR2(50) NOT NULL,
  email       VARCHAR2(255) UNIQUE NOT NULL,
  phone       VARCHAR2(20)
) TABLESPACE data_tbs;

CREATE TABLE order_items (
  order_id    NUMBER(10) REFERENCES orders(order_id),
  product_id  NUMBER(10) REFERENCES products(product_id),
  quantity    NUMBER(10) NOT NULL,
  price       NUMBER(10, 2) NOT NULL,
  CONSTRAINT order_items_pk PRIMARY KEY (order_id, product_id)
) TABLESPACE data_tbs;
```
![](./3.png)
![](./4.png)


4. 对表插入数据
通过 FOR 循环从 1 到 100,000 遍历，逐个插入数据；使用 DBMS_RANDOM.VALUE 生成随机的价格数量值；每次循环插入一条记录到每个表中；在订单详情表插入数据时，在每次循环内部，通过查询 products 表来获取一个随机的商品ID，使用 DBMS_RANDOM.VALUE 生成随机的订单详情相关数据，尝试插入一条记录到 order_details 表中，如果找不到匹配的商品ID，则通过异常处理跳过该次循环。
```  
   -- 插入商品数据
BEGIN
FOR i IN 1..100000 LOOP
INSERT INTO products (product_id, product_name, price, quantity)
VALUES (i, 'Product ' || i, ROUND(DBMS_RANDOM.VALUE(10, 1000), 2), ROUND(DBMS_RANDOM.VALUE(1, 100), 0));
END LOOP;
COMMIT;
END;
/

-- 插入客户数据
BEGIN
FOR i IN 1..100000 LOOP
INSERT INTO customers (customer_id, customer_name, contact, address)
VALUES (i, 'Customer ' || i, 'Contact ' || i, 'Address ' || i);
END LOOP;
COMMIT;
END;
/

-- 插入订单数据
BEGIN
FOR i IN 1..100000 LOOP
INSERT INTO orders (order_id, customer_id, order_date, total_amount)
VALUES (i, ROUND(DBMS_RANDOM.VALUE(1, 100000), 0), SYSDATE - i, ROUND(DBMS_RANDOM.VALUE(10, 1000), 2));
END LOOP;
COMMIT;
END;
/

-- 插入订单详情数据
BEGIN
FOR i IN 1..100000 LOOP
DECLARE
product_id NUMBER;
BEGIN
SELECT product_id
INTO product_id
FROM products
WHERE product_id = ROUND(DBMS_RANDOM.VALUE(1, 100000), 0);
  INSERT INTO order_details (detail_id, order_id, product_id, quantity, unit_price, total_amount)
  VALUES (i, ROUND(DBMS_RANDOM.VALUE(1, 100000), 0), product_id, ROUND(DBMS_RANDOM.VALUE(1, 10), 0), ROUND(DBMS_RANDOM.VALUE(10, 100), 2), ROUND(DBMS_RANDOM.VALUE(10, 1000), 2));
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    CONTINUE;
END;
END LOOP;
COMMIT;
END;
/
```
  ![](./5.png)


5. 创建两个用户 
管理员角色被授予创建会话、创建表、创建序列和创建视图的权限。
销售员角色被授予对商品、客户、订单和订单详情表的选择、插入、更新和删除权限。
```sql
-- 创建管理员角色

CREATE ROLE admin_yj;

-- 创建销售角色
CREATE ROLE sales_yj;
```
![](./6.png)
```
-- 授予管理员角色权限
GRANT CREATE SESSION TO admin_yj;
GRANT CREATE TABLE TO admin_yj;
GRANT CREATE SEQUENCE TO admin_yj;
GRANT CREATE VIEW TO admin_yj;
GRANT ALL PRIVILEGES ON products TO admin_yj;
GRANT ALL PRIVILEGES ON customers TO admin_yj;
GRANT ALL PRIVILEGES ON orders TO admin_yj;
GRANT ALL PRIVILEGES ON order_details TO admin_yj;

-- 授予销售员角色权限
GRANT CREATE SESSION TO sales_yj;
GRANT SELECT, INSERT, UPDATE, DELETE ON products TO sales_yj;
GRANT SELECT, INSERT, UPDATE, DELETE ON customers TO sales_yj;
GRANT SELECT, INSERT, UPDATE, DELETE ON orders TO sales_yj;
GRANT SELECT, INSERT, UPDATE, DELETE ON order_details TO sales_yj;
```
![](./7.png)
![](./8.png)


6. 创建函数和存储过程 
   		创建一个名为sales_pkg的包，其中包含PROCEDURE子程序print_order_summary。此子程序将接受一个订单ID作为输入参数，然后打印关于该订单的信息，包括订单总金额、订单日期。

```
CREATE OR REPLACE PACKAGE BODY sales_pkg AS
PROCEDURE print_order_summary(p_orderid IN NUMBER) IS
    l_order_total NUMBER := 0;
    l_order_date DATE;
    l_product_name VARCHAR2(100);
    l_quantity NUMBER;
    l_price NUMBER(10, 2);
  BEGIN

-- 计算总金额
    SELECT SUM(od.quantity * p.price)
    INTO l_order_total
    FROM orders o
    JOIN ordertetails od ON o.orderid = od.orderid
    JOIN products p ON od.productid = p.productid
    WHERE o.orderid = p_orderid;
    
    -- 获取日期
    SELECT orderdate
    INTO l_order_date
    FROM orders
    WHERE orderid = p_orderid;
END;
/
```
   ![](./9.png)

7. 完成数据库的数据备份 

   ​		为确保数据的安全性和可恢复性，我们设计了一个数据库备份方案。首先，我们将数据库切换到归档日志模式，这样可以确保所有的日志记录都被保存下来。然后，我们使用RMAN（Recovery Manager）对整个数据库进行备份。

   我们定义了一个RMAN备份脚本，该脚本能够完整备份整个数据库。脚本包括了自动备份控制文件、备份集的保留策略、备份优化以及删除过时备份集等设置。
   
```
# RMAN Configuration File

# Set the default backup destination
CONFIGURE DEFAULT DEVICE TYPE TO DISK;

# Set the backup retention policy (14 days)
CONFIGURE RETENTION POLICY TO RECOVERY WINDOW OF 14 DAYS;

# Set the default backup type to full backup
CONFIGURE BACKUP OPTIMIZATION OFF;
CONFIGURE CONTROLFILE AUTOBACKUP ON;
CONFIGURE DEVICE TYPE DISK PARALLELISM 4 BACKUP TYPE TO BACKUPSET;

# Set the backup encryption password (optional)
CONFIGURE ENCRYPTION FOR DATABASE ON;
CONFIGURE ENCRYPTION ALGORITHM 'AES192';
CONFIGURE ENCRYPTION USING 'AES192' IDENTIFIED BY password;

# Set the backup compression option (optional)
CONFIGURE DEVICE TYPE DISK COMPRESSION ALGORITHM 'BZIP2';
```

```sql

sqlplus / as sysdba
SHUTDOWN IMMEDIATE
STARTUP MOUNT
ALTER DATABASE ARCHIVELOG；
ALTER DATABASE OPEN；

```
![](./10.png)

## 总结与心得
Oracle数据库是一个功能强大的关系型数据库，可以用于各种规模和类型的应用程序。在这个任务中，我建立了一个基于Oracle数据库的商品销售系统的数据设计方案，并学习了备份和恢复操作。

在设计数据库方案时，我首先需要确定使用的表和表空间，然后创建相应的用户并授权。使用PL/SQL编写存储过程和函数可以实现复杂的业务逻辑，并使用循环插入大量数据。

备份和恢复是Oracle管理和维护数据库的重要部分。我了解到，Oracle支持多种备份和恢复技术，例如RMAN和Oracle Data Pump，我们可以根据需要选择它们中的任何一个来创建备份和恢复操作。在使用RMAN备份数据库时，我们应该先定义好备份方案，然后通过预备份和备份操作来实现备份，最后通过验证操作来验证备份数据的正确性。在恢复操作中，正确的操作顺序是关闭数据库，将数据库设置为mount

