# 202010414323 杨杰
#  实验1:SQL语句的执行计划分析与优化指导
#  实验目的
## 分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用
# 实验数据库和用户
## 数据库是pdborcl，用户是sys和hr
# 实验内容
* 对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
* 设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。
# 实验过程
## 1:用户hr默认没有统计权限，要向用户hr授予以下视图的选择权限
 v_$sesstat, v_$statname 和 v_$session

 * 权限分配过程如下
 ```SQL

 $ sqlplus sys/123@localhost/pdborcl as sysdba
@$ORACLE_HOME/sqlplus/admin/plustrce.sql
create role plustrace;
GRANT SELECT ON v_$sesstat TO plustrace;
GRANT SELECT ON v_$statname TO plustrace;
GRANT SELECT ON v_$mystat TO plustrace;
GRANT plustrace TO dba WITH ADMIN OPTION;
GRANT plustrace TO hr;
GRANT SELECT ON v_$sql TO hr;
GRANT SELECT ON v_$sql_plan TO hr;
GRANT SELECT ON v_$sql_plan_statistics_all TO hr;
GRANT SELECT ON v_$session TO hr;
GRANT SELECT ON v_$parameter TO hr; 
GRANT ADVISOR TO hr;
```
## 2：查询语句1
```SQL

set autotrace on

SELECT d.department_name,count(e.job_id)as "部门总人数",
avg(e.salary)as "平均工资"
from hr.departments d,hr.employees e
where d.department_id = e.department_id
and d.department_name in ('IT','Sales')
GROUP BY d.department_name;

## 运行结果

Predicate Information (identified by operation id):
---------------------------------------------------

   4 - filter("D"."DEPARTMENT_NAME"='IT' OR "D"."DEPARTMENT_NAME"='Sales')
   5 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")

Note
-----
   - this is an adaptive plan


统计信息
----------------------------------------------------------
	183  recursive calls
	  0  db block gets
	331  consistent gets
	  2  physical reads
	  0  redo size
	797  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	 13  sorts (memory)
	  0  sorts (disk)
	  2  rows processed

```


## 查询语句2
```SQL
set autotrace on

SELECT d.department_name,count(e.job_id)as "部门总人数",
avg(e.salary)as "平均工资"
FROM hr.departments d,hr.employees e
WHERE d.department_id = e.department_id
GROUP BY d.department_name
HAVING d.department_name in ('IT','Sales');

## 运行结果

Predicate Information (identified by operation id):
---------------------------------------------------

   1 - filter("D"."DEPARTMENT_NAME"='IT' OR "D"."DEPARTMENT_NAME"='Sales')
   6 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")
       filter("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")


统计信息
----------------------------------------------------------
	  8  recursive calls
	  0  db block gets
	 11  consistent gets
	  5  physical reads
	  0  redo size
	797  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	  1  sorts (memory)
	  0  sorts (disk)
	  2  rows processed

```

# 查询对比
## 1：consistent gets
* Consistent gets 与db block gets的区别很明显，后者指DML访问，前者指SELECT访问，这里的一致性读不表示从回滚块组织一致性数据，意思是以保证一致性为目标的读。
* 对于逻辑读来说，一般都是基于Logical Reads= Consistent Gets + DB Block Gets
* 第一条sql语句该值远大于第二条，一定程度上可以认为第二条查询语句好一点

## 2：physical reads
*  Physical Reads通常是我们最关心的，如果这个值很高，说明要从磁盘请求大量的数据到Buffer Cache里，通常意味着系统里存在大量全表扫描的SQL语句，这会影响到数据库的性能，因此尽量避免语句做全表扫描，对于全表扫描的SQL语句，建议增 加相关的索引，优化SQL语句来解决
* 关于physical reads ，db block gets 和consistent gets这三个参数之间有一个换算公式：

       数据缓冲区的使用命中率=1 - ( physical reads / (db block gets + consistent gets) )

* 两条语句的该值都很小

## 3：sorts(memory)、sorts(disk)
* sorts(memory) 是在SORT_AREA_SIZE 中的排序操作的数量( 由于是在SORT_AREA_SIZE 中，因此不需要在磁盘进行排序)
* sorts(disk) 则是由于排序所需空间太大，SORT_AREA_SIZE 不能满足而不得不在磁盘进行排序操作的数量
* 这两项统计通常用于计算In-memory sort ratio。In-memory sort ratio 表示内存中完成的排序所占的比例
* 第二条查询语句小于第一条

# 最后认为第二条sql语句优于第一条

# 对第二条sql语句进行sql优化指导工具

## 未显示建议

# 对第一条sql语句进行sql优化指导工具
## 建议为：考虑运行可以改进物理方案设计的访问指导或者创建推荐的索引